#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    File:     server.py
    Version:  0.0.1
    Author:   fuboqing
    Email:    fubuqing@huobi.com
    Date:     2017/23/03 15:10
    Copy:     Copyright 2017, huobi.com
"""

import tornado.ioloop
import tornado.web


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")


def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ])


if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
